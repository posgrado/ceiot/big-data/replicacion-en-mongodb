[[_TOC_]]
# Introducción
Este repositorio aloja una práctica para observar el funcionamiento de un replicaset de mongodb

# Prerequisitos
1. git
1. docker y docker-compose

# ¿Cómo usar este repositorio?
## Configurar replicaset y cargar datos
1. En una terminal, clonar este repositorio:
```sh
git clone https://gitlab.com/posgrado/ceiot/big-data/replicacion-en-mongodb.git
```
2. En la misma terminal, entrar al directorio e iniciar los contenedores:
```sh
cd replicacion-en-mongodb
docker-compose up
```
3. En una nueva terminal, conectarse a una de las instancias y configuarar el replicaset:
```sh
cd replicacion-en-mongodb
mongo --port 27020 --shell ./scripts/replicaset_config.js
```
4. En una tercera terminal, conectarse a la otra instancia y permitir que reciba lecturas como secundario:
```sh
mongo --port 27021 --eval "rs.slaveOk()" --shell
```
5. Una cuarta terminal será usada para cargar datos dentro del resplicaset
```sh
mongo --port 27020 facts.js
mongo --port 27020 facts.js
mongo --port 27020 facts.js
mongo --port 27020 facts.js
```

## Ver datos en instancias mongodb
1. La segunda terminal que usamos, ha quedado conectada a una instancia, allí podemos ver las bases de datos
y verificar que se ha creado `finanzas`.
```js
show dbs
use finanzas
db.facturas.count()
```
2. En la tercera terminal, podemos hacer lo mismo para verificar que los datos fueron replicados
```js
show dbs
use finanzas
db.facturas.count()
```
3. En la cuarta terminal, podemos detener la instancia primaria de mongodb
```sh
docker-compose stop primary
```
4. En la tercera terminal, instancia secundaria, ahora se puede observar `PRIMARY` en el _prompt_.
Esta instancia se convirtió en primaria luego de que detuvimos la primera. Ahora podemos insertar 
datos en esta:
```js
db.facturas.insert({X:1})
db.facturas.count()
```
5. En la cuarta terminal, se puede volver a iniciar la primera instancia de mongodb
```sh
docker-compose start primary
```
6. En la segunda terminal, `mongo shell` se volverá a conectar, pero ahora se observa `SECONDARY`.
Es decir, al volver a entrar la instancia se convirtió en secundaria y automáticamente agregó
el último registro cargado directamente a la segunda instancia:
```js
db.facturas.count()
```
## Video explicativo
A continuación se puede ver el proceso anterior:
![](video/replicaset_shell.mov)
