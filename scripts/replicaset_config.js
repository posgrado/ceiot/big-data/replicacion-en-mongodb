cfg = {
    _id:"rs",
    members:[
        {_id:0, host:"primary:27017"},
        {_id:1, host:"secondary:27017"},
        {_id:2, host:"arbiter:27017", arbiterOnly:true}
    ]
}
rs.initiate(cfg)
rs.status()